# Arch Linux changed its "base"

How to clone this repo:

```
git clone https://gitlab.com/reborn-os-team/rebornos-cosmic-packages/old/full-base-cosmic-reborn.git
```

See in:

https://www.archlinux.org/news/base-group-replaced-by-mandatory-base-package-manual-intervention-required/

`base` group replaced by mandatory `base` package - manual intervention required

2019-10-06 - Robin Broda

The base group has been replaced by a metapackage of the same name. We advise users to install this package (pacman -Syu base), as it is effectively mandatory from now on.

Users requesting support are expected to be running a system with the base package.

Addendum:
Be aware that base as it stands does not currently contain:
- A kernel
- An editor
... and other software that you might expect. You will have to install these separately on new installations.



This new base includes the following (at the time of this writing, the file is "base-2-1-any.pkg.tar.xz"):

filesystem

gcc-libs

glibc

bash

coreutils

file

findutils

gawk

grep

procps-ng

sed

tar

gettext

pciutils

psmisc

shadow

util-linux

bzip2

gzip

xz

licenses

pacman

systemd

systemd-sysvcompat

iputils

iproute2



It is for this change that the "full-base-cosmic-reborn" file is created, to include everything necessary when installing RebornOS.

